#!/usr/bin/python3
# -*- coding: utf-8 -*-
import judge_tools.test_driver as jtest

languages = {
  "C":  jtest.Language(
    name = "C",
    extension = "c",
    source_name = "{directory}-{solution}.{extension}",
    executable_name = "{directory}-{solution}.exe",
    compile_script = [["gcc", "-O2", "-o", "{executable}", "{source}"]],
    execute_script = [["./{executable}"]]),

  "C++":  jtest.Language(
    name = "C++",
    extension = "cpp",
    source_name = "{directory}-{solution}.{extension}",
    executable_name = "{directory}-{solution}.exe",
    compile_script = [["g++", "-O2", "-o", "{executable}", "{source}"]],
    execute_script = [["./{executable}"]]),

  "Java":  jtest.Language(
    name = "Java",
    extension = "java",
    source_name = "{directory}-{solution}.{extension}",
    executable_name = "Main.class",
    compile_script = [["javac", "-g:none", "{source}"]],
    execute_script = [["java", "-server", "-Xss8m", "-Xmx512m", "Main", "{input}", "{output}"]]),

  "python2":  jtest.Language(
    name = "python2",
    extension = "py2",
    source_name = "{directory}-{solution}.{extension}",
    execute_script = [["python2",  "{source}"]]),

  "python3":  jtest.Language(
    name = "python3",
    extension = "py3",
    source_name = "{directory}-{solution}.{extension}",
    execute_script = [["python3",  "{source}"]]),
}

problems = {
  "<problem-name>": jtest.Problem(
    name = "<problem-name>",
    directory = "<problem-directory>",
    generator = [["./generator-<problem-name>.py", "{problem}.in"]], #{problem} will be replaced with <problem-name>
    validator = [["./validator-<problem-name>.py", "{problem}.in"]],
    valid_solution = jtest.Solution(
      name = "<problem-name>-<author-name>",
      language = languages["C++"],
      redirect_stdin = True,
      redirect_stdout = True),
    test_solutions = (jtest.Solution(
        name = "<problem-name>-<author-name>",
        language = languages["Java"],
        redirect_stdin = True,
        redirect_stdout = True),
    )),
}
jtest.test_problems(problems)
