#!/usr/bin/python3
# -*- coding: utf-8 -*-
import judge_tools.input_validator as jval
import sys

MIN_T, MAX_T = 1, 50
#Any other constant value you need,
#specially if it is a constraint

validator = jval.create_validator(sys.argv[1])
T = int(validator.readline(r"^[1-9][0-9]*\n$", desc = "T line"))
validator.check(MIN_T <= T <= MAX_T, "T={0} is invalid".format(T))
for Ti in range(1, T + 1):
  #Validation code for each case in the input
