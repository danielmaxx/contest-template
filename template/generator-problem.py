#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
import judge_tools.case_count_generator as jgen
import judge_tools.random_utils as jrand
import random

MIN_T, MAX_T = 1, 100
#Any other constant value you need,
#specially if it is a constraint

def write_case(output, ...):
  #Code for write ONE random case
  output.write("")

def random_case(output):
  #Code for generate ONE random case 
  write_case(output, ...)

#for i in range(3, len(sys.argv)): jgen.add_file(file = sys.argv[i])
jgen.add_function(f = lambda _, output: random_case(output = output), n = MAX_T)
jgen.generate_cases(output = open(sys.argv[1], "wt"))
