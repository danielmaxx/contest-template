==============================================
Framework for creating new programming contest
==============================================

:Autor:
	"Daniel Ampuero" <danielmaxx@gmail.com>

:Versión: 1.0 13/07/2012

The next documents explains the directory scheme used on this framework.
Please read it before you start.


problems
--------
  Everything related with the problems should be placed here. Every problem
  should have its own directory. The directory name will be the problem 
  "code" name. By instance, if a there is a problem called "George Quest",
  the directory could be named "quest". Inside each directory, there will
  be the following files and directories (taking as example the problem quest):

  ::

    -- quest
      | quest-someperson1.cpp
      | quest-somperson2.java
      | quest-someperson3.py
      | generator-quest.py
      | sample.in
      | sample.out
      | validator-quest.py
      | -- statement
          | quest.tex
          | -- images
              | quest.png


  Obviously, not all problems directory should have solutions from every problem
  setter, but it is suggested that every solution had its author name as a suffix.

  In order to save time on this mechanic task, there is an script called "mkproblem.sh"
  who will create this directory structure for you. It will be explained later.

judge_tools
-----------
  You should dowload judge_tools from https://bitbucket.org/danielmaxx/judge-tools.git
  and install it on your system with:
  
  ::  

    $ cd judge_tools
    $ sudo python3 setup.py install
  
  NOTE: You need Python 3 installed first.

statement
---------
  This directory should be updated with "update-statement.sh" script. You should
  not modify anything else than "problems.tex" file, where you must include each
  one of the problems files.
  
  Whenever you want to generate a pdf copy of the statement, simply run "generate.sh".

template
--------
  This directory contains a empty problem template. If you type:
  
  ::

    $ ./mkproblem <problem-name>
  
  an empty problem will be copied in the problems directory. After that, you must
  change the names inside "problems/<problem-name>".

judge_tools-configuration.py
----------------------------
  This is the configuration file for judge_tools. In order to run it, you must:

  ::

    $ ./judge_tools-configuration.py

mkproblem.sh
------------
  See "template"

update-statement
----------------
  See "statement"


